// ==========Ẩn hiện input==========
$(document).ready(function(){
 	$('.input-search').hide();
 	$('.icon-search').click(function(){
 		$('.input-search').toggle(200);
 	});
 });

// ==========Menu ở dưới khi ở Mobile=========
if ($(window).width() > 425) { 
	$('.navbar').addClass('navbar-static-top'); 
} else { 
	$('.navbar').addClass('navbar-fixed-bottom'); 
}
